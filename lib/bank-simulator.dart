import 'dart:math';
import 'dart:collection';

class Customer {
  double _arrivalTime = -1;
  double _waitingTime = 0;

  double alpha;
  double beta;
  double _taskDuration = 0;

  Customer({
    required this.alpha,
    required this.beta,
    required arrivalTime,
  }) {
    _arrivalTime = arrivalTime;
    _taskDuration = calculateTaskDuration(alpha, beta);
  }

  double calculateTaskDuration(alpha, beta) {
    double x = Random().nextDouble();
    return 200.0 * pow(x, alpha - 1) * pow((1 - x), beta - 1);
  }

  void calculateWaitingTime(t_i) {
    _waitingTime = t_i - _arrivalTime;
  }

  double getTaskDuration() => _taskDuration;
  double getArrivalTime() => _arrivalTime;
  double getTotalWaitingTime() => _waitingTime;
}

class BankSimulation {
  double arrivalAlpha = 100.0;
  double simulationTime = 0;

  Queue<Customer> arrivalQ = Queue();
  Queue<Customer> departureQ = Queue();

  int steps; //Simulation steps

  double t_i = 0;
  double expectedFinishTime = double.infinity; //Bank teller
  double nextQueueEventTime = 0;

  double alpha;
  double beta;

  double avgWaitingTime = 0;
  double maxWaitingTime = 0;
  double avgQueueSize = 0;
  int maxQueueSize = 0;

  List<int> queueSizesList = [];

  BankSimulation({this.alpha = 2, this.beta = 4, this.steps = 100});

  double getCustomerInterArrivalTime() {
    //Re-arranging for time, t, given a random probability value.
    return -arrivalAlpha * log(1 - Random().nextDouble());
  }

  void newCustomer() {
    t_i = nextQueueEventTime;

    arrivalQ.add(
        Customer(alpha: alpha, beta: beta, arrivalTime: nextQueueEventTime));
  }

  void progress() {
    // print("Ti $t_i, Q lenght: ${arrivalQ.length}");
    // print("nxtQE: $nextQueueEventTime expFinish $expectedFinishTime");

    if (nextQueueEventTime < expectedFinishTime) {
      // print("Customer Arrives to Queue");
      newCustomer();
      nextQueueEventTime = getCustomerInterArrivalTime() + nextQueueEventTime;
      queueSizesList.add(arrivalQ.length);
    } else {
      t_i = expectedFinishTime;

      var currentCustomer = arrivalQ.removeFirst();
      currentCustomer.calculateWaitingTime(t_i);
      departureQ.add(currentCustomer);
      // print("task duration ${currentCustomer._taskDuration}");
      //
      if (arrivalQ.isEmpty) {
        // print("Q is empty");
        newCustomer();
        expectedFinishTime = t_i + currentCustomer.getTaskDuration();
      } else {
        expectedFinishTime += currentCustomer.getTaskDuration();
      }

      // print("Finished Processing Customer, Q lenght: ${arrivalQ.length}");

    }
  }

  void startSimulation() {
    //Start simulation with 1 customer that arrives at the Q at time 0
    var customer1 =
        Customer(alpha: alpha, beta: beta, arrivalTime: nextQueueEventTime);

    arrivalQ.add(customer1);
    nextQueueEventTime = getCustomerInterArrivalTime();
    // print("task duration ${customer1._taskDuration}");
    expectedFinishTime = customer1._taskDuration + t_i;

    for (var i = 1; i <= steps; i++) {
      progress();
    }

    calculateUserQueueStatistics();
  }

  void calculateUserQueueStatistics() {
    var waitingTimes =
        departureQ.map((customer) => customer.getTotalWaitingTime()).toList();

    avgWaitingTime = waitingTimes.reduce((a, b) => a + b) / waitingTimes.length;
    maxWaitingTime = waitingTimes.reduce((max));

    avgQueueSize =
        queueSizesList.reduce((a, b) => a + b) / queueSizesList.length;
    maxQueueSize = queueSizesList.reduce((max));
  }

  void printUserStatistics() {
    print(
        "Average waiting time: ${avgWaitingTime.toStringAsPrecision(3)} Max waiting time: ${maxWaitingTime.toStringAsPrecision(3)}, Total Number of customers Served: ${departureQ.length}");
  }

  void printQueueStatistics() {
    print(
        "Average customer queue size: ${avgQueueSize.toStringAsPrecision(3)}, Max customer queue size ${maxQueueSize.toStringAsPrecision(3)}");
  }

  double getMeanMaxDifference() {
    return maxWaitingTime - avgWaitingTime;
  }
}
