import 'lib/bank-simulator.dart' as bank_sim;

void main(List<String> arguments) {
  var deltaMeanMaxMap = Map();

  print("\nFirst Question, Only Yellow Customers:");
  var bSim_y = bank_sim.BankSimulation(alpha: 2, beta: 5, steps: 100000);
  bSim_y.startSimulation();
  bSim_y.printUserStatistics();
  deltaMeanMaxMap['Yellow'] = bSim_y.getMeanMaxDifference();
  print(" ");

  print("Second Question, Only Red Customers:");
  var bSim_r = bank_sim.BankSimulation(alpha: 2, beta: 2, steps: 100000);
  bSim_r.startSimulation();
  bSim_r.printQueueStatistics();
  deltaMeanMaxMap['Red'] = bSim_r.getMeanMaxDifference();
  print(" ");

  var bSim_b = bank_sim.BankSimulation(alpha: 5.0, beta: 1.0, steps: 100000);
  bSim_b.startSimulation();
  deltaMeanMaxMap['Blue'] = bSim_b.getMeanMaxDifference();

  var val = double.infinity;
  var customer;
  deltaMeanMaxMap.forEach((k, v) {
    if (v < val) {
      val = v;
      customer = k;
    }
  });

  print(
      "Third question answer: $customer customers gives the closest value between the average and maximum customer waiting times");
}
